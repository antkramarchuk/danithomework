import gulp from "gulp";
import prefix from "gulp-autoprefixer";
import clean from "gulp-clean";
import minCss from "gulp-clean-css";
import concat from "gulp-concat";
import imgMin from "gulp-imagemin";
// import livereload from "gulp-livereload";
import dartSass from "sass";
import gulpSass from "gulp-sass";

import sync, { watch } from "browser-sync";
import uglify from "gulp-uglify";

const sass = gulpSass(dartSass);

function scss() {
  return (
    gulp
      .src("./src/scss/**.scss")
      .pipe(sass().on("error", sass.logError))
      .pipe(prefix())
      .pipe(minCss())
      // .pipe(clean())
      .pipe(concat("styles.min.css"))
      .pipe(gulp.dest("./dist/style/"))
  );
}
function js() {
  return gulp
    .src("./src/js/**.js")
    .pipe(uglify())
    .pipe(concat("script.min.js"))
    .pipe(gulp.dest("./dist/js"));
}
gulp.task("js", js);
gulp.task("scss", scss);

function serverWatch() {
  sync.init({
    server: { baseDir: "./" },
  });
  watch("./src/scss/**/*.scss", gulp.series("scss")).on("change", sync.reload);
  watch("./src/js/**/*.js", gulp.series("js")).on("change", sync.reload);
}
gulp.task("dev", serverWatch);

gulp.task("minimage", () => {
  return gulp
    .src("./src/img/**/*")
    .pipe(imgMin())
    .pipe(gulp.dest("./dist/img/"));
});
gulp.task("clean", () => {
  return gulp.src("./dist").pipe(clean());
});

gulp.task("build", gulp.series("clean", "scss", "minimage", "js"));
