const burger = document.querySelector(".header__nav__burger i   ");
console.log(burger);
burger.addEventListener("click", () => {
  document
    .querySelector(".header__nav__list")
    .classList.toggle("header__nav__list--active");
  burger.classList.toggle("fa-bars");
  burger.classList.toggle("fa-times");
});
