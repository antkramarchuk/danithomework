"use strict";
// Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// Створіть гетери та сеттери для цих властивостей.
// Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  set name(name) {
    this._name = name;
  }
  set age(age) {
    this._age = age;
  }
  set salary(salary) {
    this._salary = salary;
  }
  get name() {
    return this._name;
  }
  get age() {
    return this._age;
  }
  get salary() {
    return this._salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  get salary() {
    return this._salary * 3;
  }
}

const user1 = new Employee("Anton", 29, 500);
const user2 = new Programmer("Roman", 20, 100, "UA");
const user3 = new Programmer("Vasilii", 33, 1500, "EU");
const user4 = new Programmer("Victoriya", 18, 2000, "BG");

console.log(user2.salary, user3.salary, user4.salary);
