// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
// наприклад , щоб коли із  серверу приходили не вірні дані . сайт не ляг
// наприклад. коли в функцію передають не вірні параметри . щоб сайт не ляг

// ----------------------------------------------------------------------------------

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];
const root = document.querySelector("#root");

const requiredFields = ["price", "name", "author"];
root.innerHTML = books
  .map((e) => {
    const objectFieds = Object.keys(e);

    try {
      requiredFields.forEach((el) => {
        if (!objectFieds.includes(el)) {
          throw new Error(`field ${el} is not defined`);
        }
      });
      return `<ul><li>${e.name} --- ${e.author} ----${e.price}</li></ul>`;
    } catch (error) {
      console.log(error);
    }
  })
  .join("");
