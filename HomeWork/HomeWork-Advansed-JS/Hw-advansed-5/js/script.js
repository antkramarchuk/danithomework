class Post {
  constructor(id, title, body, name, email) {
    this.id = id;
    this.title = title;
    this.body = body;
    this.name = name;
    this.email = email;
  }
  renderPost() {
    return `  

    <div class="post" id = ${this.id} >
    <button class="post__delete" onclick="deletePost(${this.id})">Delete Post</button>
    <p class="post__text">${this.title}</p>
    <h3 class="post__title">${this.body}</h3>
    <p class="post__user__name">${this.name}</p>
    <p class="post__user_mail">${this.email}</p>
  </div> `;
  }
}

async function renderTwitter() {
  const response = await fetch("https://ajax.test-danit.com/api/json/posts");
  const posts = await response.json();
  const responseUsers = await fetch(
    "https://ajax.test-danit.com/api/json/users"
  );
  const users = await responseUsers.json();
  const postsWithUserInfo = posts.map((item) => {
    let userInfo = users.find((e) => {
      return e.id == item.userId;
    });
    return {
      ...item,
      userInfo: { ...userInfo },
    };
  });
  console.log(postsWithUserInfo);
  document.querySelector(".post").innerHTML = postsWithUserInfo
    .map((public) => {
      // console.log(public);
      const post = new Post(
        public.id,
        public.title,
        public.body,
        public.userInfo.name,
        public.userInfo.email
      );
      return post.renderPost();
    })
    .join("");
}
renderTwitter();

async function deletePost(id) {
  const response = await fetch(
    `https://ajax.test-danit.com/api/json/posts/${id}`,
    { method: "DELETE" }
  );
  if (response.status == 200) {
    document.getElementById(`${id}`).remove();
  }
}
