const ul = document.querySelector(".star-world");

fetch(`https://ajax.test-danit.com/api/swapi/films`)
  .then((res) => res.json())
  .then((res) => {
    res.forEach((films) => {
      ul.innerHTML += `
                  <li id="numer${films.id}">
                      <p class="name">Films:${films.name}</p>
                      <ul "></ul>
                      <p class="film">Episode:${films.episodeId}</p>
                    <p class="openingCrawl">Description:${films.openingCrawl}</p>
                    </li>
                     `;

      films.characters.forEach((actors) => {
        fetch(actors)
          .then((res) => res.json())
          .then((res) => {
            document.querySelector(
              `#numer${films.id}`
            ).innerHTML += `<p class="acotr">Actor:${res.name}</p>`;
          });
      });
    });
  });
