// У файлі index.html лежить розмітка двох полів вводу пароля.
// Після натискання на іконку поруч із конкретним полем - повинні відображатися символи, які ввів користувач, іконка змінює свій зовнішній вигляд. У коментарях під іконкою - інша іконка, саме вона повинна відображатися замість поточної.
// Коли пароля не видно - іконка поля має виглядати як та, що в першому полі (Ввести пароль)
// Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)
// Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
// Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – You are welcome;
// Якщо значення не збігаються - вивести під другим полем текст червоного кольору Потрібно ввести однакові значення
// Після натискання на кнопку сторінка не повинна перезавантажуватись
// Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.

const icon = document.querySelectorAll(".icon-password");

const btn = document.querySelector(".btn");
icon.forEach((e) => {
  e.addEventListener("click", () => {
    e.classList.toggle("fa-eye-slash");
    const inputType = e.previousElementSibling.getAttribute("type");

    if (inputType === "password") {
      e.previousElementSibling.setAttribute("type", "text");
    } else {
      e.previousElementSibling.setAttribute("type", "password");
    }
  });
});

function comparePassword() {
  const passwordOne = document.querySelector("#input-type-one");
  const passwordTwo = document.querySelector("#input-type-two");
  return passwordOne.value === passwordTwo.value;
}
btn.addEventListener("click", submitFunction);

function submitFunction(event) {
  event.preventDefault();
  const isValid = comparePassword();
  if (isValid) {
    alert(" You are welcome");
    document.querySelector(".wrong-pasword").style.opacity = 0;
  } else {
    document.querySelector(".wrong-pasword").style.opacity = 1;
  }
}
