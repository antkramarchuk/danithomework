// Кожна кнопка містить назву клавіші на клавіатурі
// Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною. Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.

document.body.addEventListener("keyup", (event) => {
  const btn = document.querySelectorAll(".btn");
  btn.forEach((item) => {
    console.log(item.innerText, event.code, item);
    if (
      "Key" + item.innerText === event.code ||
      item.innerText === event.code
    ) {
      item.classList.add("active");
    } else {
      item.classList.remove("active");
    }
  });
});
