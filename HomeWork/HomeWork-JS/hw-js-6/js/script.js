// Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
// При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
// Створити метод getAge() який повертатиме скільки користувачеві років.
// Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
// Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.

let firstName;
let lastName;
let birthday;
function createNewUser(firstName, lastName) {
  lastName = prompt("you Name");
  firstName = prompt("you lastName");
  birthday = prompt("you age", "dd.mm.yyyy");
  let splitBirthday = birthday.split(".");

  const newUser = {
    firstName,
    lastName,
    birthday,
    getLogin() {
      return (this.firstName[0] + this.lastName).toLowerCase();
    },
    getAge() {
      let birthdaymilisecond = new Date(
        splitBirthday[2],
        splitBirthday[1],
        splitBirthday[0]
      ).getTime();
      let dataNow = new Date().getTime();
      let differenceDate = dataNow - birthdaymilisecond;

      let differenceDeys = Math.floor(
        differenceDate / (24 * 3600 * 365.25 * 1000)
      );

      return differenceDeys;
    },
    getPassword() {
      return (
        firstName[0].toUpperCase() + lastName.toLowerCase() + splitBirthday[2]
      );
    },
  };
  return newUser;
}

console.log(createNewUser().getPassword());

// ---------------------------------------------
