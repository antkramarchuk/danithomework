"use strict";

const btn = document.querySelector(".btn-toggle");

const currentTheme = localStorage.getItem("theme");

if (currentTheme == "dark") {
  document.body.classList.add("invert");
}

btn.addEventListener("click", function () {
  document.body.classList.toggle("invert");

  let theme = "light";

  if (document.body.classList.contains("invert")) {
    theme = "dark";
  }

  localStorage.setItem("theme", theme);
});
