"use strict";
// Код для завдань лежить в папці project.

// Знайти всі параграфи на сторінці та встановити колір фону #ff0000

// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

// Встановіть в якості контента елемента з класом testParagraph наступний параграф -

// This is a paragraph

// Отримати елементи

// , вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.





// ------------------------------------------------------------------------------------------------------------------




let paragraf = document.querySelectorAll("p");
console.log(paragraf);

paragraf.forEach((element) => {
  element.style.backgroundColor = "#ff0000";
});

let searchId = document.getElementById("optionsList");
console.log(searchId);

let searchParentElement = searchId.parentElement;
console.log(searchParentElement);

let searchChild = searchId.childNodes;
console.log(searchChild);

let editContent = (document.getElementById("testParagraph").innerText =
  "This is a paragraph");
console.log(editContent);

let searchClass = document.querySelector(".main-header");
for (const iterator of searchClass.children) {
  iterator.classList.add("nav-item");
}
console.log(searchClass.children);

let remuveClass = document.querySelectorAll(".section-title");
remuveClass.forEach((elemetn) => {
  elemetn.classList.remove("section-title");
});
console.log(remuveClass);
