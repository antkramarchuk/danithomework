// Реалізувати перемикання вкладок (таби) на чистому Javascript.

// Технічні вимоги:
// У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
// Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
// Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.
// Література:
// Використання data-* атрибутів

const tabs = document.querySelector(".tabs");

function addClass(tabItem) {
  if (document.querySelector(".active")) {
    document.querySelector(".active").classList.remove("active");
  }
  tabItem.classList.add("active");
}
tabs.addEventListener("click", (event) => {
  addClass(event.target);

  const tabToShow = document.getElementById(event.target.dataset.tab);
  console.log(tabToShow);
  document.querySelectorAll(".tab").forEach((element) => {
    element.classList.add("hidden");
  });
  tabToShow.classList.remove("hidden");
});
