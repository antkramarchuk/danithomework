"use strict";

// ----------------------------------------Hwjs-------------------------------------------
function fibonachi(f0, f1, n) {
  if (n <= 1) {
    return f0 + f1;
  } else {
    return fibonachi(f1, f0 + f1, n - 1);
  }
}
console.log(fibonachi(1, 2, 1)); // 3
console.log(fibonachi(1, 2, 2)); // 5
console.log(fibonachi(1, 2, 3)); // 8
console.log(fibonachi(1, 2, 4)); // 13

function f(a, b, n) {
  n--;
  let c = a + b;
  if (n) {
    let newa = b;
    let newb = a + b;
    c = f(newa, newb, n);
  }
  return c;
}
