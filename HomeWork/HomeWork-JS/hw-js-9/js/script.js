"use strict";
// Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
//     кожен із елементів масиву вивести на сторінку у вигляді пункту списку;
//     Приклади масивів, які можна виводити на екран:

//     ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
//     ["1", "2", "3", "sea", "user", 23];
//     Можна взяти будь-який інший масив.
//     Необов'язкове завдання підвищеної складності:
//     Додайте обробку вкладених масивів. Якщо всередині масиву одним із елементів буде ще один масив, виводити його як вкладений список. Приклад такого масиву:

//     ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
//     Підказка: використовуйте map для обходу масиву та рекурсію, щоб обробити вкладені масиви.
//     Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки.

let liList = document.createElement("li");
console.log(liList);
let li = document.getElementsByTagName("li");
console.log(li);
li.insertAdjacentHTML("beforebegin", liList);
let second = 3;
let counter = document.querySelector(".count");

function showUserText(arr, parent = document.body) {
  const ulList = document.createElement("ul");
  ulList.classList.add("item");

  parent.insertAdjacentElement("afterbegin", ulList);

  arr.forEach((element) => {
    if (Array.isArray(element)) {
      element.forEach((elementArr) => {
        const nestedLi = document.createElement("li");
        nestedLi.style.marginLeft = "10px";
        nestedLi.textContent = `${elementArr}`;
        ulList.append(nestedLi);
      });
    } else {
      const liList = document.createElement("li");
      liList.classList.add("list");
      liList.textContent = `${element}`;
      ulList.append(liList);
    }
  });
}
showUserText([
  "Kharkiv",
  "Kiev",
  ["Borispol", "Irpin"],
  "Odessa",
  "Lviv",
  "Dnieper",
]);

function clearPage() {
  counter.innerHTML = second;
  if (second === 0) {
    return (document.body.innerHTML = "");
  }
  second--;

  setTimeout(clearPage, 1000);
}
clearPage();
