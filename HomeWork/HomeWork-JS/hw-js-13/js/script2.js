// Реалізувати програму, яка циклічно показує різні картинки. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// У папці banners лежить HTML код та папка з картинками.
// При запуску програми на екрані має відображатись перша картинка.
// Через 3 секунди замість неї має бути показано друга картинка.
// Ще через 3 секунди – третя.
// Ще через 3 секунди – четверта.
// Після того, як будуть показані всі картинки - цей цикл має розпочатися наново.
// Після запуску програми десь на екрані має з'явитись кнопка з написом Припинити.
// Після натискання на кнопку Припинити цикл завершується, на екрані залишається показаною та картинка, яка була там при натисканні кнопки.
// Поруч із кнопкою Припинити має бути кнопка Відновити показ, при натисканні якої цикл триває з тієї картинки, яка в даний момент показана на екрані.
// Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
// Необов'язкове завдання підвищеної складності
// При запуску програми на екрані повинен бути таймер з секундами та мілісекундами, що показує, скільки залишилося до показу наступної картинки.
// Робити приховування картинки та показ нової картинки поступовим (анімація fadeOut/fadeIn) протягом 0.5 секунди.
// Література:
// setTimeout и setInterval
let newTag = document.createElement("p");
console.log(newTag);
newTag.classList.add("time-milisecond");
document.body.append(timeVisual, newTag);

let timer = 0;
let seconds = 3;
let timerInerval = 0;
let milisecond = 100;
let milisecondIntervalId = 0;
const btn = document.querySelector(".slider-off-btn");
const btnOn = document.querySelector(".slider-on-btn");
btnOn.addEventListener("click", () => {
  makeTimer();
  timerInerval = setInterval(() => {
    seconds = seconds - 1;
    if (!seconds) {
      seconds = 3;
    }
    document.getElementById("timeVisual").innerHTML = seconds;
  }, 1000);
});

btn.addEventListener("click", () => {
  clearInterval(timer);
  clearInterval(timerInerval);
});

let slideIndex = 1;
showSlides(slideIndex);

function showSlides(n) {
  let slides = document.getElementsByClassName("hiden");
  if (n > slides.length) {
    slideIndex = 1;
  }
  if (n < 1) {
    slideIndex = slides.length;
  }
  for (let slide of slides) {
    // slide.style.display = "none";
    slide.style.opacity = "0";
  }
  slides[slideIndex - 1].style.display = "flex";
  slides[slideIndex - 1].style.opacity = "1";
}
makeTimer(); //Создаем интервал
function makeTimer() {
  timer = setInterval(function () {
    slideIndex++;
    showSlides(slideIndex);
  }, 3000);
}

// Таймер, (чтобы было удобнее считать время) можно удалить
timerInerval = setInterval(() => {
  seconds = seconds - 1;
  if (!seconds) {
    seconds = 3;
  }
  document.getElementById("timeVisual").innerHTML = seconds;
}, 1000);

milisecondIntervalId = setInterval(() => {
  milisecond = milisecond - 1;
  if (!milisecond) {
    milisecond = 100;
  }

  newTag.innerHTML = milisecond;
}, 10);
