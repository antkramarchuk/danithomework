import React from "react";

export default function Button1({ text, backgroundColor, onClick }) {
  return (
    <button
      className="bth-1"
      style={{ backgroundColor: backgroundColor, width: "100px" }}
      onClick={onClick}
    >
      {text}
    </button>
  );
}
