import React from "react";

export default function Button2({ text, backgroundColor, onClick }) {
  return (
    <button
      className="bth-2"
      style={{ backgroundColor: backgroundColor, width: "100px" }}
      onClick={onClick}
    >
      {text}
    </button>
  );
}
