import { useState } from "react";
import Button1 from "./components/Button1/Button1";
import Modal2 from "./components/Modal2/Modal2";
import Button2 from "./components/Button2/Button2";
import Modal1 from "./components/Modal1/Modal1";

function App() {
  const [isModalOpenSecuond, setModalOpenSecuond] = useState(false);
  const handleClickSecuond = () => {
    setModalOpenSecuond((prev) => !prev);
  };
  const handleConfirmSecuond = () => {
    alert("confirm logic");
    setModalOpenSecuond(false);
  };
  const handleCloseSecuond = () => {
    console.log(" close");
    setModalOpenSecuond(false);
  };

  const [isModalOpen, setModalOpen] = useState(false);
  const handleClick = () => {
    setModalOpen((prev) => !prev);
  };
  const handleConfirm = () => {
    alert("confirm logic");
    setModalOpen(false);
  };
  const handleClose = () => {
    console.log(" close");
    setModalOpen(false);
  };
  return (
    <>
      <Button1
        text="Open first modal"
        backgroundColor="red"
        onClick={handleClick}
      />
      {isModalOpen && (
        <Modal1
          isOpen={isModalOpen}
          onClose={handleClose}
          closeButton
          text="HI"
          header="REACT MODAL 1"
          actions={
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <Button1
                text="close"
                backgroundColor="red"
                onClick={handleClose}
              />
              <Button1
                text="ok"
                backgroundColor="green"
                onClick={handleConfirm}
              />
            </div>
          }
        />
      )}

      <Button2
        text="Open Secuon modal"
        backgroundColor="green"
        onClick={handleClickSecuond}
      />
      {isModalOpenSecuond && (
        <Modal2
          onClick={(event) => event.stopPropagation()}
          isOpen={isModalOpenSecuond}
          onClose={handleCloseSecuond}
          closeButton
          text="React"
          header="REACT MODAL 2"
          actions={
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <Button2
                text="close"
                backgroundColor="blue"
                onClick={handleCloseSecuond}
              />
              <Button2
                text="ok"
                backgroundColor="green"
                onClick={handleConfirmSecuond}
              />
            </div>
          }
        />
      )}
    </>
  );
}

export default App;
