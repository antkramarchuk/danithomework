import { BrowserRouter } from "react-router-dom";
import CardList from "../CardList/CardList";
import Button from "../Button/Button";
import Modal1 from "../Modal1/Modal1";
import { useState } from "react";
const Basket = (props) => {
  const [openModalDelet, setIsModalDelet] = useState(false);

  const deleteBasket = () => {
    setIsModalDelet((openModalDelet) => !openModalDelet);
  };
  const handleClose = () => {
    setIsModalDelet(false);
  };
  const deleteModalBasket = (id) => {
    deleteBasket();
    props.handleDeleteBasket(id);
  };
  return (
    <>
      <div className="list--cards">
        {props.basket.map((item) => {
          return (
            <div key={item.id}>
              {openModalDelet && (
                <Modal1
                  isOpen={openModalDelet}
                  closeButton
                  text="DELETE BASKET?"
                  header={item.name}
                  actions={
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                      }}
                    >
                      <Button
                        text="close"
                        backgroundColor="red"
                        onClick={handleClose}
                      />
                      <Button
                        text="ok"
                        backgroundColor="green"
                        onClick={() => deleteModalBasket(item.id)}
                      />
                    </div>
                  }
                />
              )}
              <div className="cards">
                <img
                  alt=""
                  style={{ width: "200px" }}
                  src={item.url}
                  className="cards-img"
                />
                <button onClick={deleteBasket} className="cards--btn--clouse">
                  X
                </button>
                <p className="cards--name">{item.name}</p>
                <p className="cards--color">{item.color}</p>
                <p className="cards--price">{item.price}</p>
                <p className="cards--id">{item.id}</p>
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
};
export default Basket;
