import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import Button from "./components/Button/Button";
import Modal1 from "./components/Modal1/Modal1";
import Nav from "./components/Nav/Nav";
import CardList from "./components/CardList/CardList";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Basket from "./components/Basket/Basket";
import "./App.css";
import Favorite from "./components/Favorite/Favorite";

function App() {
  const dispatch = useDispatch();
  // const [goods, setGoods] = useState([]);
  // const [isModalOpenSecuond, setModalOpenSecuond] = useState(false);
  const [isModalOpen, setModalOpen] = useState(false);
  const [liked, setLiked] = useState([]);
  const [itemToAdd, setItemToAdd] = useState(null);
  const [basketItems, setBasketItems] = useState([]);
  useEffect(() => {
    const basketItems = JSON.parse(localStorage.getItem("products")) || [];
    const liked = JSON.parse(localStorage.getItem("likes")) || [];
    fetch("./goods.json")
      .then((response) => response.json())
      .then((response) => dispatch({ type: "GET_GOODS", payload: response }));
    setLiked(liked);
    setBasketItems(basketItems);
    // dispatch({ type: "GET_GOODS", payload: goods });
    dispatch({ type: "GET_PRODUCTS", payload: basketItems });
    dispatch({ type: "SET_AUTHORIZED", payload: true });
    dispatch({ type: "TRACKING_MODAL", payload: isModalOpen });
  }, []);

  const handleClick = (item) => {
    setItemToAdd(item);
    setModalOpen(true);
  };
  const handleConfirm = () => {
    const newProducts = [...basketItems, itemToAdd];
    localStorage.setItem("products", JSON.stringify(newProducts));
    setBasketItems(newProducts);
    setItemToAdd(null);
    setModalOpen(false);
  };
  const handleClose = () => {
    setItemToAdd(null);
    setModalOpen(false);
  };
  const handleAddLike = (element) => {
    const localStorageLikes = JSON.parse(localStorage.getItem("likes")) || [];
    const newLikes = [...localStorageLikes, element];
    localStorage.setItem("likes", JSON.stringify(newLikes));
    setLiked(newLikes);
  };
  const handleDeleteLike = (id) => {
    const localStorageLikes = JSON.parse(localStorage.getItem("likes")) || [];
    const filteredLikes = localStorageLikes.filter((item) => item.id !== id);
    localStorage.setItem("likes", JSON.stringify(filteredLikes));
    setLiked(filteredLikes);
  };
  const handleDeleteBasket = (id) => {
    const filteredBasket = basketItems.filter((item) => item.id !== id);
    localStorage.setItem("products", JSON.stringify(filteredBasket));
    setBasketItems(filteredBasket);
  };
  return (
    <div>
      <BrowserRouter>
        <div className="nav">
          <Nav likeCount={liked.length} basketCount={basketItems.length} />
        </div>
        <Routes>
          <Route
            path="/"
            element={
              <>
                {" "}
                {isModalOpen && (
                  <Modal1
                    isOpen={isModalOpen}
                    onClose={handleClose}
                    closeButton
                    text="HI"
                    header="REACT MODAL 1"
                    actions={
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-between",
                        }}
                      >
                        <Button
                          text="close"
                          backgroundColor="red"
                          onClick={handleClose}
                        />
                        <Button
                          text="ok"
                          backgroundColor="green"
                          onClick={handleConfirm}
                        />
                      </div>
                    }
                  />
                )}
                <CardList
                  handleDeleteBasket={handleDeleteBasket}
                  handleDeleteLike={handleDeleteLike}
                  liked={liked}
                  basketItems={basketItems}
                  onClick={handleClick}
                  handleAddLike={handleAddLike}
                />
              </>
            }
          />

          <Route
            path="/basket"
            element={
              <>
                <Basket
                  handleClose={handleClose}
                  basket={basketItems}
                  handleDeleteBasket={handleDeleteBasket}
                  handleDeleteLike={handleDeleteLike}
                  liked={liked}
                  basketItems={basketItems}
                  onClick={handleClick}
                  handleAddLike={handleAddLike}
                />
              </>
            }
          />
          <Route
            path="/favorite"
            element={
              <Favorite liked={liked} handleDeleteLike={handleDeleteLike} />
            }
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
