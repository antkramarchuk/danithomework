const intialState = { profile: null, isAuthorized: false };

export const userReducer = (state = intialState, action) => {
  switch (action.type) {
    case "GET_PROFILE":
      return (state = { ...state, profile: action.payload });
    case "SET_AUTHORIZED":
      return (state = { ...state, isAuthorized: action.payload });
    default:
      return state;
  }
};
