import React from "react";
import "./style.css";
function Modal1({ closeButton, text, header, actions, onClose }) {
  return (
    <div onClick={onClose}>
      <div className="container">
        <div
          className="content-modal-1"
          onClick={(event) => event.stopPropagation()}
        >
          {closeButton ? (
            <span className="close" onClick={onClose}>
              X
            </span>
          ) : null}
          <h1>{header}</h1>
          <p>{text}</p>
          <div>{actions}</div>
        </div>
      </div>
    </div>
  );
}
export default Modal1;
