import React from "react";
import Button from "../Button/Button";
export class Card extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isLiked: false, isInBasket: false };
    console.log(this.state);
  }

  handleClick = (element) => {
    if (this.state.isLiked) {
      this.props.handleDeleteLike(element.id);
    } else {
      this.props.handleAddLike(element);
    }
  };

  handleClickBasket = (element) => {
    if (this.state.isInBasket) {
      this.props.handleDeleteBasket(element.id);
    } else {
      this.props.onClick(element);
    }
  };

  componentDidMount() {
    const isLiked = !!this.props.liked.find(
      (item) => item.id === this.props.element.id
    );
    console.log(isLiked);
    this.setState({ isLiked: isLiked });
    const isBasket = !!this.props.basketItems.find(
      (item) => item.id === this.props.element.id
    );
    this.setState({ isInBasket: isBasket });
  }

  componentDidUpdate(prevProps, prevState) {
    const isLiked = !!this.props.liked.find(
      (item) => item.id === this.props.element.id
    );
    const isBasket = !!this.props.basketItems.find(
      (item) => item.id === this.props.element.id
    );
    if (prevState.isLiked !== isLiked) {
      this.setState({ isLiked: isLiked });
    }
    if (prevState.isInBasket !== isBasket) {
      this.setState({ isInBasket: isBasket });
    }
  }
  render() {
    return (
      <>
        <div className="cards">
          <img
            alt=""
            style={{ width: "200px" }}
            src={this.props.element.url}
            className="cards-img"
          />
          <p className="cards--name">{this.props.element.name}</p>
          <p className="cards--color">{this.props.element.color}</p>
          <p className="cards--price">{this.props.element.price}</p>
          <p className="cards--id">{this.props.element.id}</p>
          <div>
            <Button
              disabled={this.state.isInBasket}
              text={this.state.isInBasket ? "In Bascet" : "Add to cart"}
              backgroundColor="silver"
              onClick={() => this.handleClickBasket(this.props.element)}
            />

            <div></div>
            <svg
              style={{
                width: "40px",
                height: "40px",
              }}
              onClick={() => this.handleClick(this.props.element)}
              version="1.0"
              xmlns="http://www.w3.org/2000/svg"
              width="1280.000000pt"
              height="1189.000000pt"
              viewBox="0 0 1280.000000 1189.000000"
              preserveAspectRatio="xMidYMid meet"
            >
              <metadata>
                Created by potrace 1.15, written by Peter Selinger 2001-2017
              </metadata>
              <g
                transform="translate(0.000000,1189.000000) scale(0.100000,-0.100000)"
                fill={this.state.isLiked ? "red" : "black"}
                stroke="none"
              >
                <path
                  d="M3250 11884 c-25 -2 -106 -11 -180 -20 -1485 -172 -2704 -1295 -3001
-2764 -133 -660 -67 -1507 171 -2223 252 -753 675 -1411 1397 -2172 342 -360
634 -630 1588 -1470 231 -203 488 -430 570 -505 1024 -920 1735 -1692 2346
-2547 l130 -183 132 0 132 1 130 192 c557 822 1212 1560 2185 2461 191 178
408 373 1027 923 956 852 1445 1343 1841 1850 643 825 968 1603 1064 2553 19
196 17 665 -5 835 -105 805 -441 1497 -998 2054 -557 557 -1250 894 -2054 998
-193 24 -613 24 -810 0 -733 -93 -1379 -387 -1920 -874 -191 -172 -406 -417
-535 -610 -30 -45 -57 -82 -60 -82 -3 0 -30 37 -60 82 -129 193 -344 438 -535
610 -531 478 -1170 773 -1878 867 -146 20 -562 34 -677 24z"
                />
              </g>
            </svg>
          </div>
        </div>
      </>
    );
  }
}
