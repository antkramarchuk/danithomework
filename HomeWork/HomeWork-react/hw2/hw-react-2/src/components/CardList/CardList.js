import React from "react";
import { Card } from "../Card/Card";

export class CardList extends React.Component {
  render() {
    return (
      <>
        <div className="list--cards">
          {this.props.goods.map((element) => (
            <Card
              handleDeleteBasket={this.props.handleDeleteBasket}
              handleDeleteLike={this.props.handleDeleteLike}
              basketItems={this.props.basketItems}
              liked={this.props.liked}
              handleAddLike={this.props.handleAddLike}
              key={element.id}
              element={element}
              onClick={this.props.onClick}
            />
          ))}
        </div>
      </>
    );
  }
}
