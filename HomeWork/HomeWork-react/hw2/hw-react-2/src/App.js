import { useEffect, useState } from "react";
import Button from "./components/Button/Button";
import Modal2 from "./components/Modal2/Modal2";
import Modal1 from "./components/Modal1/Modal1";
import { Basket } from "./components/Basket/Basket";
import "./App.css";
import { CardList } from "./components/CardList/CardList";

function App() {
  const [goods, setGoods] = useState([]);
  const [isModalOpenSecuond, setModalOpenSecuond] = useState(false);
  const [isModalOpen, setModalOpen] = useState(false);
  const [liked, setLiked] = useState([]);
  const [itemToAdd, setItemToAdd] = useState(null);
  const [basketItems, setBasketItems] = useState([]);
  useEffect(() => {
    const basketItems = JSON.parse(localStorage.getItem("products")) || [];
    const liked = JSON.parse(localStorage.getItem("likes")) || [];
    fetch("./goods.json")
      .then((response) => response.json())
      .then((response) => setGoods(response));
    setLiked(liked);
    setBasketItems(basketItems);
  }, []);

  const handleConfirmSecuond = () => {
    alert("confirm logic");
    setModalOpenSecuond(false);
  };
  const handleCloseSecuond = () => {
    console.log(" close");
    setModalOpenSecuond(false);
  };

  const handleClick = (item) => {
    setItemToAdd(item);
    setModalOpen(true);
  };
  const handleConfirm = () => {
    const newProducts = [...basketItems, itemToAdd];
    localStorage.setItem("products", JSON.stringify(newProducts));
    setBasketItems(newProducts);

    setItemToAdd(null);
    setModalOpen(false);
  };
  const handleClose = () => {
    setItemToAdd(null);
    setModalOpen(false);
  };
  const handleAddLike = (element) => {
    const localStorageLikes = JSON.parse(localStorage.getItem("likes")) || [];
    const newLikes = [...localStorageLikes, element];
    localStorage.setItem("likes", JSON.stringify(newLikes));
    setLiked(newLikes);
  };
  const handleDeleteLike = (id) => {
    const localStorageLikes = JSON.parse(localStorage.getItem("likes")) || [];
    const filteredLikes = localStorageLikes.filter((item) => item.id !== id);
    localStorage.setItem("likes", JSON.stringify(filteredLikes));
    setLiked(filteredLikes);
  };
  const handleDeleteBasket = (id) => {
    const filteredBasket = basketItems.filter((item) => item.id !== id);
    localStorage.setItem("products", JSON.stringify(filteredBasket));
    setBasketItems(filteredBasket);
  };
  return (
    <div>
      <div className="nav">
        <Basket likeCount={liked.length} basketCount={basketItems.length} />
      </div>
      {isModalOpen && (
        <Modal1
          isOpen={isModalOpen}
          onClose={handleClose}
          closeButton
          text="HI"
          header="REACT MODAL 1"
          actions={
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <Button
                text="close"
                backgroundColor="red"
                onClick={handleClose}
              />
              <Button
                text="ok"
                backgroundColor="green"
                onClick={handleConfirm}
              />
            </div>
          }
        />
      )}

      {isModalOpenSecuond && (
        <Modal2
          onClick={(event) => event.stopPropagation()}
          isOpen={isModalOpenSecuond}
          onClose={handleCloseSecuond}
          closeButton
          text="React"
          header="REACT MODAL 2"
          actions={
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <Button
                text="close"
                backgroundColor="blue"
                onClick={handleCloseSecuond}
              />
              <Button
                text="ok"
                backgroundColor="green"
                onClick={handleConfirmSecuond}
              />
            </div>
          }
        />
      )}
      <CardList
        handleDeleteBasket={handleDeleteBasket}
        handleDeleteLike={handleDeleteLike}
        liked={liked}
        basketItems={basketItems}
        onClick={handleClick}
        goods={goods}
        handleAddLike={handleAddLike}
      />
    </div>
  );
}

export default App;
