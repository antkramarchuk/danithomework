import React, { useEffect, useState } from "react";
import Button from "../Button/Button";
const Card = (props) => {
  const [isLiked, setIsLiked] = useState(false);
  const [isInBasket, setIsInBasket] = useState(false);

  const handleClick = (element) => {
    if (isLiked) {
      props.handleDeleteLike(element.id);
    } else {
      props.handleAddLike(element);
    }
  };

  const handleClickBasket = (element) => {
    if (isInBasket) {
      props.handleDeleteBasket(element.id);
    } else {
      props.onClick(element);
    }
  };

  useEffect(() => {
    const isLiked = !!props.liked.find((item) => item.id === props.element.id);

    setIsLiked(isLiked);
    const isBasket = !!props.basketItems.find(
      (item) => item.id === props.element.id
    );
    setIsInBasket(isBasket);
  });

  useEffect(() => {
    const isLiked = !!props.liked.find((item) => item.id === props.element.id);

    const isBasket = !!props.basketItems.find(
      (item) => item.id === props.element.id
    );
    if (isLiked !== isLiked) {
      setIsLiked(isLiked);
    }
    if (isInBasket !== isBasket) {
      setIsInBasket(isBasket);
    }
  });

  return (
    <>
      <div className="cards">
        <img
          alt=""
          style={{ width: "200px" }}
          src={props.element.url}
          className="cards-img"
        />
        <p className="cards--name">{props.element.name}</p>
        <p className="cards--color">{props.element.color}</p>
        <p className="cards--price">{props.element.price}</p>
        <p className="cards--id">{props.element.id}</p>
        <div>
          <Button
            disabled={isInBasket}
            text={isInBasket ? "In Bascet" : "Add to cart"}
            backgroundColor="silver"
            onClick={() => handleClickBasket(props.element)}
          />

          <div></div>
          <svg
            style={{
              width: "40px",
              height: "40px",
            }}
            onClick={() => handleClick(props.element)}
            version="1.0"
            xmlns="http://www.w3.org/2000/svg"
            width="1280.000000pt"
            height="1189.000000pt"
            viewBox="0 0 1280.000000 1189.000000"
            preserveAspectRatio="xMidYMid meet"
          >
            <metadata>
              Created by potrace 1.15, written by Peter Selinger 2001-2017
            </metadata>
            <g
              transform="translate(0.000000,1189.000000) scale(0.100000,-0.100000)"
              fill={isLiked ? "red" : "black"}
              stroke="none"
            >
              <path
                d="M3250 11884 c-25 -2 -106 -11 -180 -20 -1485 -172 -2704 -1295 -3001
-2764 -133 -660 -67 -1507 171 -2223 252 -753 675 -1411 1397 -2172 342 -360
634 -630 1588 -1470 231 -203 488 -430 570 -505 1024 -920 1735 -1692 2346
-2547 l130 -183 132 0 132 1 130 192 c557 822 1212 1560 2185 2461 191 178
408 373 1027 923 956 852 1445 1343 1841 1850 643 825 968 1603 1064 2553 19
196 17 665 -5 835 -105 805 -441 1497 -998 2054 -557 557 -1250 894 -2054 998
-193 24 -613 24 -810 0 -733 -93 -1379 -387 -1920 -874 -191 -172 -406 -417
-535 -610 -30 -45 -57 -82 -60 -82 -3 0 -30 37 -60 82 -129 193 -344 438 -535
610 -531 478 -1170 773 -1878 867 -146 20 -562 34 -677 24z"
              />
            </g>
          </svg>
        </div>
      </div>
    </>
  );
};
export default Card;
