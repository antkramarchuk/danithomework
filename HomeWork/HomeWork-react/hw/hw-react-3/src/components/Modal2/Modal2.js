import React from "react";
import "./style.css";
function Modal2({ closeButton, text, header, actions, onClose }) {
  return (
    <div onClick={onClose}>
      <div className="container2">
        <div
          className="content-modal-2"
          onClick={(event) => event.stopPropagation()}
        >
          {closeButton ? (
            <span className="close2" onClick={onClose}>
              X
            </span>
          ) : null}
          <h1>{header}</h1>
          <p>{text}</p>
          <div>{actions}</div>
        </div>
      </div>
    </div>
  );
}
export default Modal2;
