import React from "react";
import Card from "../Card/Card";

const CardList = (props) => {
  return (
    <>
      <div className="list--cards">
        {props.goods.map((element) => (
          <Card
            handleDeleteBasket={props.handleDeleteBasket}
            handleDeleteLike={props.handleDeleteLike}
            basketItems={props.basketItems}
            liked={props.liked}
            handleAddLike={props.handleAddLike}
            key={element.id}
            element={element}
            onClick={props.onClick}
          />
        ))}
      </div>
    </>
  );
};
export default CardList;
