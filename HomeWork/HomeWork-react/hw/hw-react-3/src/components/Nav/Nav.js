import React from "react";

const Nav = (props) => {
  return (
    <div className="container--basket">
      <div className="nav--favorit">
        <img
          className="basket--basket"
          alt="#"
          style={{ width: "40px" }}
          src="./img/basket.svg"
        />
        <p className="counetr--like">{props.basketCount}</p>
        <img
          className="basket-like"
          alt="#"
          style={{ width: "40px" }}
          src="./img/like.svg"
        />
        <p className="counetr--basket">{props.likeCount}</p>
      </div>
      <p className="logo">BYnow</p>
    </div>
  );
};
export default Nav;
