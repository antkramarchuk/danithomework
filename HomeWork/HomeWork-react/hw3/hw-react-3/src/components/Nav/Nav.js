import React from "react";
import { Link, NavLink } from "react-router-dom";
const Nav = (props) => {
  return (
    <div className="container--basket">
      <div className="nav--favorit">
        <Link to="/basket">
          <img
            className="basket--basket"
            alt="#"
            style={{ width: "40px" }}
            src="./img/basket.svg"
          />
        </Link>
        <p className="counetr--like">{props.basketCount}</p>
        <Link to="/favorite">
          <img
            className="basket-like"
            alt="#"
            style={{ width: "40px" }}
            src="./img/like.svg"
          />
        </Link>
        <p className="counetr--basket">{props.likeCount}</p>
      </div>
      <Link to={"/"}>
        <h2 className="logo">BYnow</h2>
      </Link>
    </div>
  );
};
export default Nav;
