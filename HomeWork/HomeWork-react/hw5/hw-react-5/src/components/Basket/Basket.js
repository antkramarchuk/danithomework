import Button from "../Button/Button";
import Modal1 from "../Modal1/Modal1";
import { useState, useSelector } from "react";
import BascetForm from "../BascetForm/BascetForm";
const Basket = (props) => {
  const [byBascet, setBybascet] = useState([]);
  // const byGoods = () => {};
  // console.log(byBascet);
  // const form = useSelector((state) => state.form);
  // console.log(form);
  const [openModalDelet, setIsModalDelet] = useState(false);
  const deleteBasket = () => {
    setIsModalDelet((openModalDelet) => !openModalDelet);
  };
  const handleClose = () => {
    setIsModalDelet(false);
  };
  const deleteModalBasket = (id) => {
    deleteBasket();
    props.handleDeleteBasket(id);
  };
  // setBybascet({...props.basket})
  // console.log(props.basket);
  return (
    <>
      <BascetForm handleBySubmit={props.handleBySubmit} />
      <div className="list--cards-basket">
        {props.basket.map((item) => {
          return (
            <div key={item.id}>
              {openModalDelet && (
                <Modal1
                  isOpen={openModalDelet}
                  closeButton
                  text="DELETE BASKET?"
                  header={item.name}
                  actions={
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                      }}
                    >
                      <Button
                        text="close"
                        backgroundColor="red"
                        onClick={handleClose}
                      />
                      <Button
                        text="ok"
                        backgroundColor="green"
                        onClick={() => deleteModalBasket(item.id)}
                      />
                    </div>
                  }
                />
              )}
              <div className="cards">
                <img
                  alt=""
                  style={{ width: "200px" }}
                  src={item.url}
                  className="cards-img"
                />
                <button onClick={deleteBasket} className="cards--btn--clouse">
                  X
                </button>
                <p className="cards--name">{item.name}</p>
                <p className="cards--color">{item.color}</p>
                <p className="cards--price">{item.price}</p>
                <p className="cards--id">{item.id}</p>
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
};
export default Basket;
