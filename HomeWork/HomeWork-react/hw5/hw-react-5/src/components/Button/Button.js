import React from "react";

export default function Button({
  text,
  backgroundColor,
  onClick,
  disabled,
  type,
}) {
  return (
    <button
      type={type}
      className="bth-1"
      style={{ backgroundColor: backgroundColor, width: "100px" }}
      onClick={onClick}
      disabled={disabled}
    >
      {text}
    </button>
  );
}
