import { Formik, isInteger } from "formik";
import { useState } from "react";
import { useDispatch } from "react-redux";
import Button from "../Button/Button";
import "./BascetForm.css";
import * as Yup from "yup";
const BascetForm = (props) => {
  const [nameForms, setNameForms] = useState("");

  const initialValues = {
    name: "",
    secoundName: "",
    age: "",
    deliveryAddress: "",
    phone: "",
  };
  const phoneRegExp =
    /^(\+?\d{0,4})?\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{3}\)?)\s?-?\s?(\(?\d{4}\)?)?$/;
  const SignupSchema = Yup.object().shape({
    name: Yup.string()
      .min(2, "Too Short!")
      .max(50, "Too Long!")
      .required("Required"),
    secoundName: Yup.string()
      .min(2, "Too Short!")
      .max(50, "Too Long!")
      .required("Required"),
    age: Yup.number().required("Required"),
    deliveryAddress: Yup.string().required("Required"),
    phone: Yup.string()
      .matches(phoneRegExp, "Phone number is not valid")
      .required("Required"),
  });
  const submitForm = (values) => {
    props.handleBySubmit();
    console.log(values);
  };

  //   // const form = document.getElementById('form');
  // const formData = new FormData(form);

  // const output = document.getElementById('output');

  // for (const [key, value] of formData) {
  //   output.textContent += `${key}: ${value}\n`;
  // }
  // const SignInForm = () => {
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={submitForm}
      validationSchema={SignupSchema}
    >
      {(formik) => {
        // console.log("2");
        const {
          values,
          handleChange,
          handleSubmit,
          errors,
          touched,
          handleBlur,
          isValid,
          dirty,
        } = formik;
        return (
          <div className="form-container">
            <h1 className="form-title">Дані для замовлення</h1>
            <form className="form" onSubmit={handleSubmit}>
              <div className="form-row">
                <label htmlFor="name">name</label>
                <input
                  type="name"
                  name="name"
                  id="name"
                  value={values.name}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={errors.name && touched.name ? "input-error" : null}
                />
                {errors.name && touched.name && (
                  <span className="error">{errors.name}</span>
                )}
              </div>
              <div className="form-row">
                <label htmlFor="secoundName">secoundName</label>
                <input
                  type="secoundName"
                  name="secoundName"
                  id="secoundName"
                  value={values.secoundName}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.secoundName && touched.secoundName
                      ? "input-error"
                      : null
                  }
                />
                {errors.secoundName && touched.secoundName && (
                  <span className="error">{errors.secoundName}</span>
                )}
              </div>
              <div className="form-row">
                <label htmlFor="age">age</label>
                <input
                  onKeyPress={(event) => {
                    if (!/[0-9]/.test(event.key)) {
                      event.preventDefault();
                    }
                  }}
                  name="age"
                  id="age"
                  value={values.age}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={errors.age && touched.age ? "input-error" : null}
                />
                {errors.age && touched.age && (
                  <span className="error">{errors.age}</span>
                )}
              </div>
              <div className="form-row">
                <label htmlFor="deliveryAddress">deliveryAddress</label>
                <input
                  type="deliveryAddress"
                  name="deliveryAddress"
                  id="deliveryAddress"
                  value={values.deliveryAddress}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.deliveryAddress && touched.deliveryAddress
                      ? "input-error"
                      : null
                  }
                />
                {errors.deliveryAddress && touched.deliveryAddress && (
                  <span className="error">{errors.deliveryAddress}</span>
                )}
              </div>
              <div className="form-row">
                <label htmlFor="phone">phone</label>
                <input
                  type="phone"
                  name="phone"
                  id="phone"
                  value={values.phone}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  className={
                    errors.phone && touched.phone ? "input-error" : null
                  }
                />
                {errors.phone && touched.phone && (
                  <span className="error">{errors.phone}</span>
                )}
              </div>
              <Button
                className="Checkout"
                text="Checkout"
                backgroundColor="green"
                type="submit"
              />
            </form>
          </div>
        );
      }}
    </Formik>
  );
};

export default BascetForm;
