import React, { memo } from "react";
import Card from "../Card/Card";
import { useSelector } from "react-redux";
const CardList = (props) => {
  const goods = useSelector((state) => state.good.goods);

  return (
    <>
      <div className="list--cards">
        {goods.map((element) => (
          <Card
            handleDeleteBasket={props.handleDeleteBasket}
            handleDeleteLike={props.handleDeleteLike}
            basketItems={props.basketItems}
            liked={props.liked}
            handleAddLike={props.handleAddLike}
            key={element.id}
            element={element}
            onClick={props.onClick}
          />
        ))}
      </div>
    </>
  );
};
export default memo(CardList);
