import React from "react";
import { Link, NavLink } from "react-router-dom";
import { useSelector } from "react-redux";
const Nav = (props) => {
  const user = useSelector((state) => state.user.profile);
  // console.log(user);
  return (
    <div className="container--basket">
      <div className="nav--favorit">
        <Link to="/basket">
          <img
            className="basket--basket"
            alt="#"
            style={{ width: "40px" }}
            src="./img/basket.svg"
          />
        </Link>
        <p className="counetr--like">{props.basketCount}</p>
        <Link to="/favorite">
          <img
            className="basket-like"
            alt="#"
            style={{ width: "40px" }}
            src="./img/like.svg"
          />
        </Link>
        <p className="counetr--basket">{props.likeCount}</p>
      </div>
      <Link to={"/"}>
        <h2 className="logo">BYnow</h2>
      </Link>
      {user ? <p>{user.name}</p> : <button>Login</button>}
    </div>
  );
};
export default Nav;
