import { combineReducers } from "redux";
import { todoReducer } from "./todo-reducer";
import { userReducer } from "./userReducer";
import { goodsReduser } from "./goodsReduser";
import { ModalOpen } from "./tracingModal";
import { getForm } from "./getForm";
export const rootReducer = combineReducers({
  reducer: todoReducer,
  user: userReducer,
  good: goodsReduser,
  tracingModal: ModalOpen,
  form: getForm,
});
