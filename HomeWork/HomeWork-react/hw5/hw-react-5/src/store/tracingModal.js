const intialState = { ModalOpen: [] };

export const ModalOpen = (state = intialState, action) => {
  switch (action.type) {
    case "TRACKING_MODAL":
      return (state = { ...state, ModalOpen: action.payload });
    default:
      return state;
  }
};
